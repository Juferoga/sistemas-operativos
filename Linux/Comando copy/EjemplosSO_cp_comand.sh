#!/bin/bash

echo "                                         "
echo "Creación de sistema de ficheros"
echo "                                         "

mkdir -p root/imagenes
mkdir root/documentos
mkdir root/musica
mkdir root/carpeta

echo "                                         "
tree root
echo "                                         "

echo "                                         "
echo "Creación de archivos en la carpeta Root"
echo "                                         "

cd root

echo "                                         "
echo "________________________________________"
echo "Creación de imagenes"
echo "                                         "

touch imagen1.jpg
touch imagen2.jpg
touch imagen3.jpg
ls *.jpg

echo "                                         "
echo "________________________________________"
echo "Creación de documentos"
echo "                                         "

touch documento1.odt
touch documento2.odt
ls *.odt

echo "                                         "
echo "________________________________________"
echo "Creación de música"
echo "                                         "

touch mipista1.ogg
touch mipista2.ogg
ls *.ogg

echo "                                         "
echo "________________________________________"
echo "Se creara un ejemplo por tema del archivo"
echo "________________________________________"
echo "                                         "
echo "                                         "

echo "Ejemplo 1 -- copia normal"
cp -v imagen1.jpg ./imagenes/
tree

echo "                                         "
echo "________________________________________"
echo "                                         "
echo "Ejemplo 2 -- 1 o mas archivos"
echo "                                         "
cp -v imgen2.jpg imagen3.jpg ./imagenes/
tree

echo "                                         "
echo "________________________________________"
echo "                                         "
echo "Ejemplo 3 -- copiar a un directorio"
echo "                                         "
cp -v *.odt ./documentos
tree

echo "                                         "
echo "________________________________________"
echo "                                         "
echo "Ejemplo 4 -- copiar un directorio completo"
echo "                                         "
cp -vr carpeta ./musica
tree

echo "                                         "
echo "________________________________________"
echo "                                         "
echo "Ejemplo 5 -- Respaldo de un archivo"
echo "                                         "
cp -b -v mipista.ogg ./musica/
ls -l musica
tree

echo "                                         "
echo "Ejemplo 6 -- Forzar la copia"
echo "                                         "
cp -v -f imagen1.jpg ./imagenes/
tree

echo "                                         "
echo "Ejemplo 7 -- Notificacion"
echo "                                         "
cp -v mipista2.ogg ./musica/
tree
