public class Produtor implements Runnable {
  Dekker dekker;

  public Produtor(Dekker dekker) {
    this.dekker = dekker;
  }
  
  public void run() {
    while (!dekker.done) {
      
      // hace cualquier cosa antes de la region crítica
      int x = 2; for (int i=1; i<=128; i++) { x *= 2; }
      
      System.out.println("El productor quiere entrar en la region critica");
      dekker.produtorWantsToEnter = true;
      while (dekker.consumidorWantsToEnter) {
        if (dekker.favoredThread == 2) {
          dekker.produtorWantsToEnter = false;
          while (dekker.favoredThread == 2);
          dekker.produtorWantsToEnter = true;
        }
      }

      // Início de la region crítica
      System.out.println("El productor consiguio entrar na region crítica");
      // hace cualquier cosa dentro de la region crítica
      x = 2; for (int i=1; i<=256; i++) { x *= 2; }
      // Fin de la region crítica      

      dekker.favoredThread = 2;
      dekker.produtorWantsToEnter = false;
      System.out.println("El productor salio de la region crítica");
      
      try {
        Thread.sleep(1);
      } catch (InterruptedException e) {
        System.err.println("ERROR: " + e.getMessage());
      }
    }
    
    System.out.println("El productor acabo");    
  }
}
