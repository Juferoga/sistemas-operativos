# Dekker Sistemas Operativos
Implementacion en java del algoritmo de exclusion mutua de Dekker. 
El codigo representa la version numero cinco del algoritmo de Dekker(su version final), the one without the known problems of lockstep synchronization (v1) and indefinite postponement (v4).

## Integrantes

| Edwin Aaron Garcia Pulido     |  20161020    |
| Juan Felipe Rodriguez Galindo |  20181020158 |
