public class Consumidor implements Runnable {
  Dekker dekker;
  
  public Consumidor(Dekker dekker) {
    this.dekker = dekker;
  }
  
  public void run() {
    while (!dekker.done) {
      
      // hacer cualquier cosa antes de la región crítica
      int x = 3; for (int i=1; i<=64; i++) { x *= 3; }
      
      System.out.println("El consumidor quiere entrar en la región crítica");
      dekker.consumidorWantsToEnter = true;
      while (dekker.produtorWantsToEnter) {
        if (dekker.favoredThread == 1) {
          dekker.consumidorWantsToEnter = false;
          while (dekker.favoredThread == 1);
          dekker.consumidorWantsToEnter = true;
        }
      }

      // Início de la región crítica
      System.out.println("El consumidor consiguio entrar en la región crítica");
      // hacer cualquier cosa dentro de la región crítica
      float y = 100000; while (y > 0) { y /= 3; }
      // Fin de la región crítica      

      dekker.favoredThread = 1;
      dekker.consumidorWantsToEnter = false;
      System.out.println("El consumidor salio de la region critica");
      
      try {
        Thread.sleep(1);
      } catch (InterruptedException e) {
        System.err.println("ERROR: " + e.getMessage());
      }
    }
        
    System.out.println("El productor acabo");    
  }
}
