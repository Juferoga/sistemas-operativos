package logica;

public class RoundRobin extends ListaProcesos {

	public RoundRobin() {
		super();
	}

	@Override
	public void agregar(Proceso tempo) {
		if (procesos.size() == 0) {
			cola.add(tempo.getId());
			tempo.addTComienzo(0);
			procesos.add(tempo);
			procesos.get(0).addEstado(new EstadoActivo(tempo.getTLlegada()));
		} else {
			cola.add(tempo.getId());
			procesos.add(tempo);
			procesos.get(procesos.size() - 1).addEstado(new EstadoEspera(tempo.getTLlegada()));
		}
	}

	@Override
	public void finalizar(int cursor) {
		String tempo = cola.remove(0);
		getProceso(tempo).cambiarEstado(cursor, new EstadoFinalizado(cursor));
		getProceso(tempo).addTFinal(cursor);
		getProceso(tempo).setTRafagaEjec(getProceso(tempo).getTRafaga());
		if (cola.size() > 0) {
			getProceso(cola.get(0)).cambiarEstado(cursor, new EstadoActivo(cursor));
			getProceso(cola.get(0)).addTComienzo(cursor);
		}
	}

	@Override
	public void bloquear(int cursor) throws InterruptedException {
		for (int i = 0; i < procesos.size(); i++) {
			if (procesos.get(i).getId().equals(cola.get(0))) {
				procesos.get(i).addEstado(new EstadoBloqueado(cursor));
				String id = cola.remove(0);
				colaBloqueada.add(id);
				Thread.sleep(1000);
				cola.add(colaBloqueada.remove(0));
				i = procesos.size() + 1;
			}
		}
	}

	@Override
	public void esperar(int cursor) {
		getProceso(cola.get(0)).cambiarEstado(cursor, new EstadoEspera(cursor));
		getProceso(cola.get(0)).setTRafagaEjec(cursor - getProceso(cola.get(0)).getTComienzo());
		getProceso(cola.get(0)).addTFinal(cursor);

		String tempo = cola.remove(0);
		cola.add(tempo);

		getProceso(cola.get(0)).cambiarEstado(cursor, new EstadoActivo(cursor));
		getProceso(cola.get(0)).addTComienzo(cursor);

	}

	@Override
	public void activar(int cursor) {
		for (int i = 0; i < procesos.size(); i++) {
			if (procesos.get(i).getId().equals(cola.get(0)) && procesos.get(i).getEstado().equals("Espera")) {
				procesos.get(i).addEstado(new EstadoActivo(cursor));
			} else if (procesos.get(i).getId().equals(cola.get(0)) && procesos.get(i).getTLlegada() > cursor) {
				procesos.get(i).addEstado(new EstadoActivo(procesos.get(i).getTLlegada()));
				i = procesos.size() + 1;
			}
		}

	}

}
