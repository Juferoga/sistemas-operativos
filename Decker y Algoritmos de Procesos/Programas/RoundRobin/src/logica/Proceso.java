package logica;

import java.util.ArrayList;

public class Proceso {

	private String id;

	private int tLlegada;
	private int tRafaga;
	private int tRafagaEjecutada;
	private ArrayList<Integer> tComienzo;

	private int tEspera;
	private ArrayList<Integer> tRetorno;
	private ArrayList<Integer> tFinal;

	private ArrayList<Estado> estado;

	public Proceso(String id, int tLlegada, int tRafaga) {
		this.tLlegada = tLlegada;
		this.tRafaga = tRafaga;
		this.id = id;
		this.tComienzo = new ArrayList<Integer>();
		tRafagaEjecutada = 0;
		tEspera = 0;
		tRetorno = new ArrayList<Integer>();
		tFinal = new ArrayList<Integer>();
		estado = new ArrayList<Estado>();
	}

	public int getTLlegada() {
		return tLlegada;
	}

	public int getTRafaga() {
		return tRafaga;
	}

	public int getTRafagaEjec() {
		return tRafagaEjecutada;
	}

	public void setTRafagaEjec(int tRafagaEjecutada) {
		this.tRafagaEjecutada = this.tRafagaEjecutada + tRafagaEjecutada;
	}

	public int getTRafagaFalt() {
		return tRafaga - tRafagaEjecutada;
	}

	public int getTComienzo() {
		return tComienzo.get(tComienzo.size() - 1);
	}

	public ArrayList<Integer> getTInicio() {
		return tComienzo;
	}

	/*
	 * this.tFinal = t_rafaga + t_comienzo; this.tRetorno = tFinal - t_llegada;
	 * this.tEspera = tRetorno - t_rafaga;
	 */

	public void addTComienzo(int tComienzo) {
		this.tComienzo.add(tComienzo);
	}

	public void addTFinal() {
		this.tFinal.add(getTRafagaEjec() + this.tComienzo.get(this.tComienzo.size() - 1));
		this.tRetorno.add(this.tFinal.get(tFinal.size() - 1) - tLlegada);
		setTEspera(this.tRetorno.get(tRetorno.size() - 1) - tRafaga);
	}

	public int getTEspera() {
		return tEspera;
	}

	public void setTEspera(int tEspera) {
		this.tEspera = this.tEspera + tEspera;
	}

	public ArrayList<Integer> getTRetorno() {
		return tRetorno;
	}

	public void addTRetorno(int tRetorno) {
		this.tRetorno.add(tRetorno);
	}

	public ArrayList<Integer> getTFinal() {
		return tFinal;
	}

	public void addTFinal(int tFinal) {
		this.tFinal.add(tFinal);
		this.tRetorno.add(this.tFinal.get(this.tFinal.size() - 1) - tLlegada);
	}

	public String getEstado() {
		if (estado.size() == 0) {
			return null;
		} else {
			return estado.get(estado.size() - 1).nombreEstado;
		}
	}

	public ArrayList<Estado> getEstados() {
		return this.estado;
	}

	public void addEstado(Estado estado) {
		this.estado.add(estado);
	}

	public void cambiarEstado(int cursor, Estado state) {
		estado.get(estado.size() - 1).fin = cursor;
		addEstado(state);
	}

	public String getId() {
		return id;
	}
}
