package logica;

public abstract class Estado {
	public String nombreEstado;
	public int principio;
	public int fin;

	public Estado(String nombre, int principio) {
		nombreEstado = nombre;
		this.principio = principio;
		this.fin = -1;
	}
}
