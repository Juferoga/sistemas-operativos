package logica;

import java.util.ArrayList;

public abstract class ListaProcesos {
	protected ArrayList<Proceso> procesos;
	protected ArrayList<String> cola;
	protected ArrayList<String> colaBloqueada;

	public ListaProcesos() {
		procesos = new ArrayList<Proceso>();
		cola = new ArrayList<String>();
		colaBloqueada = new ArrayList<String>();
	}

	public abstract void agregar(Proceso tempo);

	public abstract void finalizar(int cursor);

	public abstract void bloquear(int cursor) throws InterruptedException;

	public abstract void esperar(int cursor);

	public abstract void activar(int cursor);

	public ArrayList<Proceso> getLista() {
		return procesos;
	}

	public ArrayList<String> getCola() {
		return cola;
	}

	public Proceso getProceso(String id) {
		for (int i = 0; i < procesos.size(); i++) {
			if (procesos.get(i).getId().equals(id)) {
				return procesos.get(i);
			}
		}
		return null;
	}
}
