package logica;

import java.awt.Graphics;

public class EstadoFinalizado extends Estado {
	public EstadoFinalizado(int fin) {
		super("Finalizado", fin);
		this.fin = fin;
	}
}
