package presentacion;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

public class PanelAdd extends JPanel {

	// Definicion de objetos Dinamicos
	private String[] etiquetas = new String[] { "Identificaci\u00F3n", "T. Llegada", "T. Rafaga"};
	private JTextField[] txtEntradas = new JTextField[etiquetas.length];
	private JLabel[] lblTexto = new JLabel[etiquetas.length];
	// Estaticos
	private JLabel lblTitlle = new JLabel("Insertar Datos");
	public JButton btnAgregar;
	// Para que funcione toca agregarla :v
	private Font font = new Font("Arial", Font.BOLD, 14);

	public PanelAdd() {
		// Sin Layout = Manejo x coordenadas
		setLayout(null);
		setBorder(new LineBorder(Color.BLACK));
		lblTitlle.setFont(font);
		lblTitlle.setBounds(100, 10, 150, 20);
		add(lblTitlle);
		
		// Dibujamos Labels y TextBox
		for (int i = 0; i < txtEntradas.length; i++) {
			lblTexto[i] = new JLabel(etiquetas[i]);
			lblTexto[i].setBounds(10, 50 + (i * 30), 145, 20);
			add(lblTexto[i]);

			txtEntradas[i] = new JTextField();
			txtEntradas[i].setBounds(175, 50 + (i * 30), 145, 20);
			add(txtEntradas[i]);
		}
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(175, 240, 145, 30);
		btnAgregar.setFont(font);
		add(btnAgregar);

	}

	public JTextField getCampoTexto(int i) {
		return txtEntradas[i];
	}

}
