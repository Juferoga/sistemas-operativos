package presentacion;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

import logica.Proceso;

public class PnlDibujo extends JPanel {
	public ArrayList<Proceso> procesos;
	public int unidades = 10;
	public int cursor = 0;

	public PnlDibujo(ArrayList<Proceso> procesos) {
		setBackground(Color.WHITE);
		setBorder(null);
		this.procesos = procesos;
		setPreferredSize(new Dimension(90 + (200 * this.unidades), 90 + (200 * this.procesos.size())));
	}

	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getPreferredSize().width, getPreferredSize().height);
		g.setColor(Color.BLACK);
		if (cursor + 3 > unidades) {
			unidades = unidades + 5;
		}
		g.drawLine(40, 40, 40 + (50 * unidades), 40);
		g.drawLine(40, 40, 40, 40 + (this.procesos.size() * 50));

		// Dibujar eje X
		for (int i = 0; i < unidades; i++) {
			g.drawLine(20 + (50 * (i + 1)), 30, 20 + (50 * (i + 1)), 40);
			g.drawString(Integer.toString(i), 15 + (50 * (i + 1)), 25);
		}

		// Dibujar eje Y
		for (int i = 0; i < this.procesos.size(); i++) {
			g.drawLine(30, 20 + (50 * (i + 1)), 40, 20 + (50 * (i + 1)));
			g.drawString(procesos.get(i).getId(), 20, 25 + (50 * (i + 1)));
		}
		for (int i = 0; i < procesos.size(); i++) {
			dibujarLinea(procesos.get(i), g, cursor, 20 + (50 * (i + 1)));
		}
	}

	public void dibujarLinea(Proceso proceso, Graphics g, int cursor, int altura) {
		for (int i = 0; i < proceso.getEstados().size(); i++) {
			int finTrazo = -1;
			int aux = 70 + (50 * proceso.getEstados().get(i).principio);

			if (proceso.getEstados().get(i).fin != -1) {
				finTrazo = proceso.getEstados().get(i).fin;
			} else {
				finTrazo = cursor;
			}

			switch (proceso.getEstados().get(i).nombreEstado) {
			case "Activo":
				g.setColor(Color.BLUE);
				g.drawLine(70 + (50 * proceso.getEstados().get(i).principio), altura, 70 + (50 * finTrazo), altura);
				break;
			case "Espera":
				g.setColor(Color.RED);
				while (aux < ((finTrazo * 50) + 70)) {
					g.drawLine(aux, altura, aux + 10, altura);
					aux = aux + 20;
				}
				break;
			case "Bloqueado":
				g.setColor(Color.BLACK);
				while (aux < ((finTrazo * 50) + 70)) {
					g.drawLine(aux, altura - 5, aux + 10, altura + 5);
					aux = aux + 20;
				}
				break;
			default:
				break;
			}

			g.drawLine(70 + (50 * proceso.getEstados().get(i).principio), altura - 5,
					70 + (50 * proceso.getEstados().get(i).principio), altura + 5);

		}
	}
}
