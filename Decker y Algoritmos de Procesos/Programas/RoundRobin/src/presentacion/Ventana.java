package presentacion;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import logica.RoundRobin;
import logica.ListaProcesos;
import logica.Proceso;

import javax.swing.JButton;

public class Ventana extends JFrame implements ActionListener {

	private JPanel contentPane;
	public boolean seleccion = true;
	public PanelAdd pnlAgregarProceso;
	public TablaProcesos table;
	public PnlDibujo pnlDibujo;
	public JButton btnSimular;
	public JButton btnBloquear;
	public int quantum = 4;
	public JScrollPane spDibujo;
	public Timer tmp = new Timer(1000, new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (fifo.getCola().size() > 0) {
				if (pnlDibujo.cursor > 0 && pnlDibujo.cursor % quantum == 0) {
					if (pnlDibujo.cursor - fifo.getProceso(fifo.getCola().get(0)).getTComienzo() < fifo
							.getProceso(fifo.getCola().get(0)).getTRafagaFalt()) {
						fifo.esperar(pnlDibujo.cursor);
						table.modificarModelo(fifo.getLista());
					} else if (pnlDibujo.cursor - fifo.getProceso(fifo.getCola().get(0)).getTComienzo() == fifo
							.getProceso(fifo.getCola().get(0)).getTRafagaFalt()) {
						fifo.finalizar(pnlDibujo.cursor);
						table.modificarModelo(fifo.getLista());
					}
				} else {
					if (pnlDibujo.cursor - fifo.getProceso(fifo.getCola().get(0)).getTComienzo() == fifo
							.getProceso(fifo.getCola().get(0)).getTRafagaFalt()) {
						fifo.finalizar(pnlDibujo.cursor);
						table.modificarModelo(fifo.getLista());
					}
				}
			}
			pnlDibujo.repaint();
			pnlDibujo.cursor++;
			pnlDibujo.setSize(new Dimension(90 + (50 * pnlDibujo.unidades), 90 + (50 * fifo.getLista().size())));
			pnlDibujo.updateUI();
			if (fifo.getCola().size() == 0) {
				tmp.stop();
				for (int i = 0; i < fifo.getLista().size(); i++) {
					for (int j = 0; j < fifo.getLista().get(i).getEstados().size(); j++) {
						if (fifo.getLista().get(i).getEstados().get(j).nombreEstado.equals("Espera")) {
							fifo.getLista().get(i).setTEspera(fifo.getLista().get(i).getEstados().get(j).fin
									- fifo.getLista().get(i).getEstados().get(j).principio);
						}
					}
				}
				table.modificarModelo(fifo.getLista());
			}

		}
	});
	ListaProcesos fifo = new RoundRobin();

	public Ventana() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(10, 10, 1150, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(new LineBorder(Color.BLACK));
		scrollPane.setBounds(20, 20, 740, 280);
		contentPane.add(scrollPane);

		pnlAgregarProceso = new PanelAdd();
		pnlAgregarProceso.setBounds(800, 20, 330, 280);
		pnlAgregarProceso.btnAgregar.addActionListener(this);
		contentPane.add(pnlAgregarProceso);

		table = new TablaProcesos(fifo.getLista());

		pnlDibujo = new PnlDibujo(fifo.getLista());
		scrollPane.setViewportView(table);

		btnSimular = new JButton("Simular");
		btnSimular.setBounds(500, 620, 150, 30);
		btnSimular.addActionListener(this);
		contentPane.add(btnSimular);

		spDibujo = new JScrollPane();
		spDibujo.setBounds(20, 320, 1130, 280);
		contentPane.add(spDibujo);
		spDibujo.setViewportView(pnlDibujo);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btnSource = (JButton) e.getSource();
		if (btnSource.equals(pnlAgregarProceso.btnAgregar)) {
			fifo.agregar(new Proceso(pnlAgregarProceso.getCampoTexto(0).getText(),
					Integer.parseInt(pnlAgregarProceso.getCampoTexto(1).getText()),
					Integer.parseInt(pnlAgregarProceso.getCampoTexto(2).getText())));
			table.modificarModelo(fifo.getLista());
		} else if (btnSource.equals(btnSimular)) {
			tmp.start();
		}
	}
}
