package logica;

public class Proceso{

	private String id;           // identificador del proceso
	private long tLlegada;    // Legada del proceso al programa
	private long tRafaga;     // Tiempo de duracion del proceso
	private long tComienzo;   // Tiempo en el que inicia el proceso
	
	// A T R I B U T O S
	private long tFinal;      // Tiempo en el que finaliza el proceso
	private long tRetorno;    // Tiempo o ciclo de vida del proceso
	private long tEspera;     // Tiempo que espera el proceso en ejecuarse
	private boolean ejecutado;

	public Proceso(String id, long t_llegada, long t_rafaga, long t_comienzo) {
		this.id = id;
		this.tLlegada = t_llegada;
		this.tRafaga = t_rafaga;
		this.tComienzo = t_comienzo;
		this.tFinal = t_rafaga + t_comienzo;
		this.tRetorno = tFinal - t_llegada;
		this.tEspera = tRetorno - t_rafaga;
		ejecutado = false;
	}

	// GETTERS & SETTERS

	public String getId() {
		return id;
	}

	public long getTLlegada() {
		return tLlegada;
	}

	public void setTLlegada(long t_llegada) {
		this.tLlegada = t_llegada;
		this.tRetorno = tFinal - this.tLlegada;
	}

	public long getTRafaga() {
		return tRafaga;
	}

	public void setTRafaga(long t_rafaga) {
		this.tRafaga = t_rafaga;
		this.tFinal = t_rafaga + tComienzo;
		this.tRetorno = this.tFinal - this.tLlegada;
		this.tEspera = tRetorno - t_rafaga;

	}

	public long getTComienzo() {
		return tComienzo;
	}

	public void setTComienzo(long t_comienzo) {
		this.tComienzo = t_comienzo;
		this.tFinal = this.tRafaga + t_comienzo;
		this.tRetorno = this.tFinal - this.tLlegada;
		this.tEspera = this.tRetorno - this.tRafaga;
	}
	// ---------------------------------------------------------- //
	//  Ya que es un tiempo que se genera ó se halla, solo se     //
	//  necesita variar; los datos fundamentales que dan opcion   //
	//  a generar los datos dependientes                          //
	// ---------------------------------------------------------- //

	public long getTFinal() {
		return tFinal;
	}

	public long getTRetorno() {
		return tRetorno;
	}

	public long getTEspera() {
		return tEspera;
	}

	public boolean isEjecutado() {
		return ejecutado;
	}

	public void setEjecutado(boolean ejecutado) {
		this.ejecutado = ejecutado;
	}
}
