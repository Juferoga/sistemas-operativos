package presentacion;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import logica.Proceso;

public class Ventana extends JFrame implements ActionListener {

	private JPanel contentPane;
	public PanelDibujo pnlDibujo;
	public PanelBloqueados pnlBloqueado;
	private TablaProcesos table;
	private ArrayList<Proceso> procesos = new ArrayList<Proceso>();
	private ArrayList<String> procesosBloqueados = new ArrayList<String>();
	private PanelAdd pnlAgregarProceso;
	private JButton btnSimular;
	private JButton btnBloquear;
	private Timer simulacion;
	private boolean bandera = true;
	boolean isBloq = false;
	public JScrollPane spDibujo;

	private ArrayList<Integer> listaOrden;

	public Ventana() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(10, 10, 1150, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		//procesos.add(new Proceso("A", 0, 8, 0));
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(new LineBorder(Color.BLACK));
		scrollPane.setBounds(10, 10, 510, 280);
		contentPane.add(scrollPane);

		//scrollPane.setViewportView(pnlDibujo);
		table = new TablaProcesos(procesos);

		scrollPane.setViewportView(table);

		pnlAgregarProceso = new PanelAdd();
		pnlAgregarProceso.setBounds(530, 10, 330, 280);
		pnlAgregarProceso.btnAgregar.addActionListener(this);
		contentPane.add(pnlAgregarProceso);

		pnlBloqueado = new PanelBloqueados(procesosBloqueados);
		pnlBloqueado.setBounds(870, 10, 270, 280);
		pnlBloqueado.setBorder(new LineBorder(Color.BLACK));
		contentPane.add(pnlBloqueado);

		pnlDibujo = new PanelDibujo(40, procesos);

		spDibujo = new JScrollPane();
		spDibujo.setBounds(10, 300, 1130, 300);
		spDibujo.setBorder(new LineBorder(Color.BLACK));
		spDibujo.setViewportView(pnlDibujo);
		contentPane.add(spDibujo);

		btnSimular = new JButton("Simular");
		btnSimular.addActionListener(this);
		btnSimular.setBounds(465, 630, 100, 30);
		add(btnSimular);

		btnBloquear = new JButton("Bloquear");
		btnBloquear.addActionListener(this);
		btnBloquear.setBounds(585, 630, 100, 30);
		add(btnBloquear);

	}

	// IMPORTANTE
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton) e.getSource();
		if (boton.equals(pnlAgregarProceso.btnAgregar)) {
			Proceso tempo = new Proceso(pnlAgregarProceso.getCampoTexto(0).getText(),
					Long.parseLong(pnlAgregarProceso.getCampoTexto(1).getText()),
					Long.parseLong(pnlAgregarProceso.getCampoTexto(2).getText()), 0);
			agregar(tempo);
			table.modificarModelo(procesos);
			for (int i = 0; i < 3; i++) {
				pnlAgregarProceso.getCampoTexto(i).setText("");
			}

			pnlDibujo.setSize(new Dimension(90 + (50 * (int) (this.procesos.get(this.procesos.size() - 1).getTFinal())),
					90 + (50 * (this.procesos.size() + 2))));
			pnlDibujo.updateUI();
			spDibujo.updateUI();
		} else if (boton.equals(btnSimular)) {
			if (bandera) {
				pnlDibujo.setProcesos(procesos);
				pnlDibujo.maximo = 0;
				simulacion = new Timer(1000, new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						pnlDibujo.repaint();
						if (isBloq) {
							int procesoActivo = getProcesoActivo();
							if (procesos.get(procesoActivo).getTFinal() == pnlDibujo.maximo) {
								JOptionPane.showMessageDialog(null,
										"No se puede bloquear, debido a que ya terminó el procedimiento");
								isBloq = false;
							} else {
								long rafagaEjecutada = (long) pnlDibujo.maximo
										- procesos.get(procesoActivo).getTComienzo();
								long rafagaFaltante = procesos.get(procesoActivo).getTRafaga() - rafagaEjecutada;
								procesos.get(procesoActivo).setTRafaga(rafagaEjecutada);
								if (procesoActivo + 1 < procesos.size()) {
									agregar(new Proceso(procesos.get(procesoActivo).getId() + "'",
											procesos.get(procesoActivo).getTFinal() + 1, rafagaFaltante, 0));
								}

								procesosBloqueados.add(procesos.get(procesos.size() - 1).getId());
								pnlBloqueado.cambiar(procesosBloqueados);
								pnlBloqueado.updateUI();
								table.modificarModelo(procesos);
								isBloq = false;
							}
						}

						pnlDibujo.maximo++;
						System.out.println(pnlDibujo.maximo);
						if (pnlDibujo.maximo > procesos.get(listaOrden.get(listaOrden.size() - 1)).getTFinal()) {
							simulacion.stop();
							bandera = true;
							pnlDibujo.repaint();
							btnSimular.setText("Simular");
						}

					}

				});
				btnSimular.setText("Parar");
				simulacion.start();
				bandera = false;
			} else {
				simulacion.stop();
				btnSimular.setText("Simular");
				pnlDibujo.maximo = 0;
				pnlDibujo.repaint();
				bandera = true;
			}
		} else if (boton.equals(btnBloquear)) {
			if (!isBloq) {
				isBloq = true;
			}
		}
	}

	private void agregar(Proceso tempo) {
		long tFinFlag = 0;

		if (listaOrden == null) {
			listaOrden = new ArrayList<Integer>();
		} else {
			for (int i = 0; i < listaOrden.size(); i++) {
				if (!procesos.get(listaOrden.get(i)).isEjecutado()) {
					listaOrden.remove(i);
					i--;
				}
			}
		}

		// variable Tamaño de Lista Orden
		int tam = listaOrden.size();
		System.out.println(tam);
		for (int i = 0; i < listaOrden.size(); i++) {
			if (procesos.get(listaOrden.get(i)).getTFinal() > tFinFlag) {
				tFinFlag = procesos.get(listaOrden.get(i)).getTFinal();
			}
		}

		procesos.add(tempo);
		ArrayList<Integer> tLlegada = new ArrayList<Integer>();
		ArrayList<Integer> tRafaga = new ArrayList<Integer>();
		for (int i = 0; i < procesos.size(); i++) {
			tLlegada.add((int) procesos.get(i).getTLlegada());
			tRafaga.add((int) procesos.get(i).getTRafaga());
		}
		Collections.sort(tLlegada);
		Collections.sort(tRafaga);
		long rafMin = 10000;
		int flag = 0;
		for (int i = 0; i < procesos.size(); i++) {
			if (procesos.get(i).getTLlegada() == tLlegada.get(0) && procesos.get(i).getTRafaga() < rafMin
					&& !procesos.get(i).isEjecutado()) {
				rafMin = procesos.get(i).getTRafaga();
				flag = i;
			}
		}

		tRafaga.remove((Integer) ((int) procesos.get(flag).getTRafaga()));
		if (listaOrden.size() == 0) {
			listaOrden.add(flag);
		}

		tFinFlag = tFinFlag + rafMin + procesos.get(flag).getTLlegada();
		while (listaOrden.size() < procesos.size()) {
			flag = 0;
			int j = 0;
			int i = 0;
			for (i = 0; i < tRafaga.size(); i++) {
				// if (tFinFlag >= tRafaga.get(i)) {
				for (j = 0; j < procesos.size(); j++) {
					if (tRafaga.get(i) == procesos.get(j).getTRafaga() && procesos.get(j).getTLlegada() <= tFinFlag
							&& !procesos.get(j).isEjecutado() && !listaOrden.contains(j)) {
						flag = j;
						j = procesos.size();
					}
				}
				if (j == procesos.size() + 1) {
					listaOrden.add(flag);
					i = tRafaga.size() + 1;
					tFinFlag = tFinFlag + procesos.get(flag).getTRafaga();
				}
				// }
			}
		}

		System.out.println("--------");
		System.out.println(listaOrden.toString());
		if (tam>0) {
			for (int i = tam-1; i < listaOrden.size()-1; i++) {
				procesos.get(listaOrden.get(i+1)).setTComienzo(procesos.get(listaOrden.get(i)).getTFinal());
			}
		} else {
			for (int i = tam; i < listaOrden.size()-1; i++) {
				procesos.get(listaOrden.get(i+1)).setTComienzo(procesos.get(listaOrden.get(i)).getTFinal());
			}
		}
		
	}

	private int getProcesoActivo() {
		int interruptor = pnlDibujo.maximo;
		for (int i = 0; i < procesos.size(); i++) {
			if (procesos.get(i).getTFinal() < interruptor) {
				System.out.println("Entre y active el proceso " + procesos.get(i).getId());
				procesos.get(i).setEjecutado(true);
			}
			if (procesos.get(i).getTComienzo() < interruptor && interruptor <= procesos.get(i).getTFinal()) {
				System.out.println("Entre y active el proceso " + procesos.get(i).getId());
				procesos.get(i).setEjecutado(true);
				return i;
			}
		}
		return -1;
	}
}
