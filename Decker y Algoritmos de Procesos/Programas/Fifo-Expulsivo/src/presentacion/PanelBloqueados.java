package presentacion;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import logica.Proceso;

public class PanelBloqueados extends JPanel {
	public ArrayList<JLabel> lblProcesos;

	public PanelBloqueados(ArrayList<String> procesosBloqueados) {
		setLayout(new GridLayout(0, 1, 0, 0));
		lblProcesos = new ArrayList<JLabel>();
		for (int j = 0; j < procesosBloqueados.size(); j++) {
			JLabel tempo = new JLabel(procesosBloqueados.get(j));
			tempo.setBorder(new LineBorder(Color.black));
			lblProcesos.add(tempo);
			add(lblProcesos.get(j));
		}
	}

	public void cambiar(ArrayList<String> procesosBloqueados) {
		for (int i = 0; i < lblProcesos.size(); i++) {
			remove(lblProcesos.get(i));
		}
		lblProcesos.clear();
		lblProcesos = new ArrayList<JLabel>();
		for (int j = 0; j < procesosBloqueados.size(); j++) {
			JLabel tempo = new JLabel(procesosBloqueados.get(j));
			tempo.setBorder(new LineBorder(Color.black));
			lblProcesos.add(tempo);
			add(lblProcesos.get(j));
		}
	}
}
