import javax.swing.JOptionPane;

import presentacion.Ventana;

public class Launcher {
	public static void main(String[] args) {
		try {
			Ventana frame = new Ventana(Long.parseLong(JOptionPane.showInputDialog("Ingrese la identificación del cajero")), Integer.parseInt(JOptionPane.showInputDialog("Ingrese la capacidad máxima del cajero")));
			frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
