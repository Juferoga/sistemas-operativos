package proceso;

public class Nodo{

	// A T R I B U T O S
	private long cedula;
	private boolean cajero;
	private int cantidad;
	private Nodo siguiente;
	
	public long getCedula() {
		return cedula;
	}
	
	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Nodo getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(Nodo siguiente) {
		this.siguiente = siguiente;
	}

	//Constructor 1: Construye un nodo tipo Cajero
	public Nodo(long id,boolean cajero, int cant) {
		this.cedula = id;
		this.cajero = cajero;
		this.cantidad = cant;
		this.siguiente = null;
	}

	//Constructor 2: Construye un nodo tipo Cliente
	public Nodo(long id, int cant) {
		this.cedula = id;
		this.cajero = false;
		this.cantidad = cant;
		this.siguiente = null;
	}

	// Verifica si el objeto es de tipo cajero
	public boolean isCajero() {
		return cajero;
	}
}
