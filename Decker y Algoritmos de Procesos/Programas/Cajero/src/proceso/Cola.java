package proceso;

public class Cola {

	private Nodo cajero;

	// Constructor de la clase cola
	public Cola(long cedula, int cantMax) {
		// Creacion de nodo Tipo Cajero
		cajero = new Nodo(cedula, true, cantMax);
		// Se añade El puntero del objeto a el objeto cajero
		cajero.setSiguiente(cajero);
	}

	public Nodo getCajero(){
		return this.cajero;
	}
	
	// Método de comprobación, comprueba si la fila esta empty
	public boolean isVacio() {
		if (cajero.equals(cajero.getSiguiente())) {
			return true;
		} else {
			return false;
		}
	}

	// Método para comprobar si el nodo que llega es el último
	public Nodo ultimo(Nodo aux) {
		// Traducción XD => Si el nodo que nos llego tiene el 
		// apuntador hacia el cajero. RETORNE el mismo apuntador
		if (aux.getSiguiente().equals(cajero)) {
			return aux;
		} else {
		// Si no se cumple, RETORNE el último con el apuntador
		// del nodo de llegada
			return ultimo(aux.getSiguiente());
		}
	}

	// Método para saber el tamaño de ...
	public int tamano() {
		// Creamos un objrto auxiliar para datos tmp
		Nodo aux = cajero;
		// Acumulador tipo tamaño
		int tamano = 0;
		// Mientras que el cajero no tenga a el mismo
		// apuntador del auxiliar
		while(!cajero.equals(aux.getSiguiente())) {
			aux = aux.getSiguiente();
			tamano++;
		}
		return tamano;
	}

	// Creación de nodo tipo Cliente
	public void addNodo(long cedula, int cantidad) {
		// Crear Cliente
		Nodo cliente = new Nodo(cedula, cantidad);
		// Verificar Cliente/Cajero anterior
		Nodo anterior = ultimo(cajero);
		// Al anterior se le da el apuntador del cliente
		// que fue creado
		anterior.setSiguiente(cliente);
		// Al cliente se le da el apuntador del
		// cliente último, es decir hacia el cajero
		cliente.setSiguiente(cajero);
	}

	// Eliminación de un Nodo
	public boolean delNodo() {
		// se verifica el método isVacio
		if(!isVacio()) {
			/*		  Grafica importante XD
						[cajero]=[C1][C2]...[Cn]
			*/
			// Creación del cajero en la posición q le sigue al cajero
			Nodo cliente = cajero.getSiguiente();
			// Creacion de un auxiliar con la posición c2 
			Nodo aux = cliente.getSiguiente();

			// Si la cantidad de procesos del cliente es menor a la cantidad
			// que puede antender el cajero
			if (cliente.getCantidad() <= cajero.getCantidad()) {
				// le da la posición de c2 al cajero
				cajero.setSiguiente(aux);
			} 
			else {
				// Si el cajero no puede atender todas las tareas del cliente

		// cantidad del cliente = la cantidad del cliente - la cantidad atendida por el cajero 
				cliente.setCantidad(cliente.getCantidad() - cajero.getCantidad());
				// ahora al ultimo le damos la posicion de cajero
				Nodo ultimo = ultimo(cajero);
				// si el último es diferente a un cliente
				if(!ultimo.equals(cliente)) {
					// le da la posición de c2 al cajero
					cajero.setSiguiente(aux);
					// le da la posición de cn-1 al cliente volviendose el c1 -> cn
					ultimo.setSiguiente(cliente);
					// le da la posición de c2 al cajero
					cliente.setSiguiente(cajero);
				}
			}
			// eliminación completa
			return true;
		}
		// si esta vacio avisa que no es posible eliminarlo
		return false;
	}
}
