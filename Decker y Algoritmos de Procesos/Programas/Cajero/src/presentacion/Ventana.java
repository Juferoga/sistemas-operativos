package presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import proceso.Cola;

import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Ventana extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField txtId;
	private JTextField txtCantProcesos;
	private JButton btnAgregar;
	private JButton btnAtender;
	//private JButton btnAtenderAuto;
	public Cola cola;
	public PanelDibujo pnlDibujo;

	public Ventana(long cedula, int cantMax) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 660, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		cola = new Cola(cedula, cantMax);
		pnlDibujo = new PanelDibujo(cola.getCajero().getCedula(), cola.getCajero().getCantidad());

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 461, 201);
		contentPane.add(scrollPane);

		scrollPane.setViewportView(pnlDibujo);

		JPanel pnlAgregar = new JPanel();
		pnlAgregar.setBounds(485, 12, 163, 201);
		contentPane.add(pnlAgregar);
		pnlAgregar.setLayout(null);

		JLabel lblIdentificacin = new JLabel("Identificación: ");
		lblIdentificacin.setBounds(12, 12, 120, 15);
		pnlAgregar.add(lblIdentificacin);

		JLabel lblTareas = new JLabel("Tareas:");
		lblTareas.setBounds(12, 101, 120, 15);
		pnlAgregar.add(lblTareas);

		txtId = new JTextField();
		txtId.setBounds(12, 39, 114, 19);
		pnlAgregar.add(txtId);

		txtCantProcesos = new JTextField();
		txtCantProcesos.setBounds(12, 128, 114, 19);
		pnlAgregar.add(txtCantProcesos);

		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(22, 164, 117, 25);
		btnAgregar.addActionListener(this);
		pnlAgregar.add(btnAgregar);

		btnAtender = new JButton("Atender");
		btnAtender.setBounds(269, 234, 117, 25);
		btnAtender.addActionListener(this);
		contentPane.add(btnAtender);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton) e.getSource();
		if (boton.equals(btnAgregar)) {
			cola.addNodo(Long.parseLong(txtId.getText()), Integer.parseInt(txtCantProcesos.getText()));
			txtId.setText("");
			txtCantProcesos.setText("");
		} else if (boton.equals(btnAtender)) {
			if (cola.isVacio()) {
				JOptionPane.showMessageDialog(null, "La fila está vacía");
			} else {
				cola.delNodo();
			}
		}
		// Se dibuja
		pnlDibujo.paintCola(pnlDibujo.getGraphics(), cola);
	}
	
}
