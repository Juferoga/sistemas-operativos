package presentacion;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import proceso.Cola;
import proceso.Nodo;

public class PanelDibujo extends JPanel {
	private long cedula;
	private int cantidad;

	public PanelDibujo(long cedula, int cantidad) {
		this.cedula = cedula;
		this.cantidad = cantidad;
	}
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());		
		g.setColor(Color.GREEN);
		g.drawRect(15, 15, 40, 40);
		g.drawString(Integer.toString(cantidad), 30, 70);
		g.drawString(Long.toString(cedula), 30, 40);
	}
	public void paintCola(Graphics g, Cola cola) {
		// Se inicializan los gráficos
		paint(g);
		
		g.setColor(Color.BLUE);
		// creamos un nodo auxiliar que guarde la posición de c1
		/*
			[cajero] = [C1] [C2] [C3]
		*/
		Nodo aux = cola.getCajero().getSiguiente();
		// Con este for dibujamos el cajero y los clientes
		for (int i = 0; i < cola.tamano(); i++) {
			// Cajero y clientes (los cuadritos)
			g.drawRect(15 + (50 * (1+i)), 15, 40, 40);
			// Dibuja la cantidad de tareas que tiene
			g.drawString(Integer.toString(aux.getCantidad()), 30 + (50 * (1+i)), 70);
			// Dibuja la identificación.
			g.drawString(Long.toString(aux.getCedula()), 30 + (50*(1+i)), 40);
			// El aux le damos la posicion que guradaba el auxiliar
			aux = aux.getSiguiente();
		}
	}
}
