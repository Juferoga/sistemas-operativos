package presentacion;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

import logica.Proceso;

public class PanelDibujo extends JPanel {
	private int unidades = 10;
	private ArrayList<Proceso> procesos = new ArrayList<Proceso>();
	public int maximo = 0;

	public PanelDibujo(int unidades, ArrayList<Proceso> procesos) {
		setBackground(Color.WHITE);
		setBorder(null);
		this.unidades = unidades;
		this.procesos = procesos;
		setPreferredSize(new Dimension(90 + (50 * this.unidades), 90 + (50 * this.procesos.size())));
	}

	public void repintar(Graphics g) {
		paint(g);
		for (int i = 0; i < procesos.size(); i++) {
			pintarLinea(g, (int) (20 + (50 * (procesos.get(i).getTLlegada() + 1))), 20 + 50 * (i + 1), true,
					(int) (procesos.get(i).getTEspera()));
			pintarLinea(g, (int) (20 + (50 * (procesos.get(i).getTComienzo() + 1))), 20 + 50 * (i + 1), false,
					(int) (procesos.get(i).getTRafaga()));
		}
	}

	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getPreferredSize().width, getPreferredSize().height);
		g.setColor(Color.BLACK);
		g.drawLine(40, 40, 40 + (50 * unidades), 40);
		g.drawLine(40, 40, 40, 40 + (this.procesos.size() * 50));

		// Dibujar eje X
		for (int i = 0; i < unidades; i++) {
			g.drawLine(20 + (50 * (i + 1)), 30, 20 + (50 * (i + 1)), 40);
			g.drawString(Integer.toString(i), 15 + (50 * (i + 1)), 25);
		}

		// Dibujar eje Y
		for (int i = 0; i < this.procesos.size(); i++) {
			g.drawLine(30, 20 + (50 * (i + 1)), 40, 20 + (50 * (i + 1)));
			g.drawString(procesos.get(i).getId(), 20, 25 + (50 * (i + 1)));
		}
		// Dibujar los procesos :D
		for (int i = 0; i < procesos.size(); i++) {
			// Pinta los procesos en espera
//			pintarLinea(g, (int) (20 + (50 * (procesos.get(i).getTLlegada() + 1))), 20 + 50 * (i + 1), true,(int) (procesos.get(i).getTEspera()));
			pintarLinea(g, (int) (20 + (50 * (procesos.get(i).getTLlegada() + 1))), 20 + 50 * (i + 1), true,
					procesos.get(i));
			pintarLinea(g, (int) (20 + (50 * (procesos.get(i).getTComienzo() + 1))), 20 + 50 * (i + 1), false,
					procesos.get(i));
			// Pinta los procesos en ejecución
//			pintarLinea(g, (int) (20 + (50 * (procesos.get(i).getTComienzo() + 1))), 20 + 50 * (i + 1), false,(int) (procesos.get(i).getTRafaga()));
		}

	}

	public void pintarLinea(Graphics g, int x, int y, boolean flag, Proceso proceso) {
		if (flag) {
			if (maximo >= (proceso.getTLlegada() + proceso.getTEspera())) {
				pintarLinea(g, x, y, true, (int) proceso.getTEspera());
			} else if (maximo < (proceso.getTLlegada() + proceso.getTEspera())) {
				if (maximo > proceso.getTLlegada()) {
					pintarLinea(g, x, y, true, (int) (maximo - proceso.getTLlegada()));
				}
			}
		} else {
			if (maximo >= (proceso.getTComienzo() + proceso.getTRafaga())) {
				pintarLinea(g, x, y, false, (int) proceso.getTRafaga());
			} else if (maximo < (proceso.getTComienzo() + proceso.getTRafaga())) {
				if (maximo > proceso.getTComienzo()) {
					pintarLinea(g, x, y, false, (int) (maximo - proceso.getTComienzo()));
				}
			}
		}

//		if () {
//			g.setColor(Color.BLUE);
//			g.drawLine(x, y, x + (50 * trayecto), y);
//		} else {
//			g.setColor(Color.RED);
//			int aux = x;
//			while (aux < ((trayecto * 50) + x)) {
//				g.drawLine(aux, y, aux + 10, y);
//				aux = aux + 20;
//			}
//		}
//		g.drawLine(x, y - 5, x, y + 5);
//		g.drawLine(x + (50 * trayecto), y - 5, x + (50 * trayecto), y + 5);
	}

	public void pintarLinea(Graphics g, int x, int y, boolean espera, int trayecto) {
		if (!espera) {
			g.setColor(Color.BLUE);
			g.drawLine(x, y, x + (50 * trayecto), y);
		} else {
			g.setColor(Color.RED);
			int aux = x;
			while (aux < ((trayecto * 50) + x)) {
				g.drawLine(aux, y, aux + 10, y);
				aux = aux + 20;
			}
		}
		
		
		g.drawLine(x, y - 5, x, y + 5);
		g.drawLine(x + (50 * trayecto), y - 5, x + (50 * trayecto), y + 5);
	}

	public void setProcesos(ArrayList<Proceso> procesos) {
		this.procesos = procesos;
		this.unidades = (int) (this.procesos.get(this.procesos.size() - 1).getTFinal()) + 4;
		setPreferredSize(new Dimension(90 + (50 * this.unidades), 90 + (50 * this.procesos.size())));
	}
}
