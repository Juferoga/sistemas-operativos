package presentacion;

import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import logica.Proceso;

public class TablaProcesos extends JTable {
	private String[] columnas = new String[] { "Identificaci\u00F3n", "T. Llegada", "T. Rafaga", "T. Comienzo",
			"T. Final", "T. Retorno", "T. Espera" };

	TablaProcesos(ArrayList<Proceso> procesos) {
		super();
		String[][] datos = new String[procesos.size()][columnas.length];
		for (int i = 0; i < procesos.size(); i++) {
			datos[i][0] = procesos.get(i).getId();
			datos[i][1] = Long.toString(procesos.get(i).getTLlegada());
			datos[i][2] = Long.toString(procesos.get(i).getTRafaga());
			datos[i][3] = Long.toString(procesos.get(i).getTComienzo());
			datos[i][4] = Long.toString(procesos.get(i).getTFinal());
			datos[i][5] = Long.toString(procesos.get(i).getTRetorno());
			datos[i][6] = Long.toString(procesos.get(i).getTEspera());
		}
		TableModel modelo = new DefaultTableModel(datos, columnas);
		setModel(modelo);
		setEnabled(false);
		getColumnModel().getColumn(0).setPreferredWidth(105);
		getColumnModel().getColumn(3).setPreferredWidth(105);
	}

	public void modificarModelo(ArrayList<Proceso> procesos) {
		String[][] datos = new String[procesos.size()][columnas.length];
		for (int i = 0; i < procesos.size(); i++) {
			datos[i][0] = procesos.get(i).getId();
			datos[i][1] = Long.toString(procesos.get(i).getTLlegada());
			datos[i][2] = Long.toString(procesos.get(i).getTRafaga());
			datos[i][3] = Long.toString(procesos.get(i).getTComienzo());
			datos[i][4] = Long.toString(procesos.get(i).getTFinal());
			datos[i][5] = Long.toString(procesos.get(i).getTRetorno());
			datos[i][6] = Long.toString(procesos.get(i).getTEspera());
		}
		TableModel modelo = new DefaultTableModel(datos, columnas);
		setModel(modelo);
	}

}
