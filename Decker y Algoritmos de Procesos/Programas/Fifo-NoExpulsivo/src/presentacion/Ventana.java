package presentacion;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import logica.Proceso;

public class Ventana extends JFrame implements ActionListener {

	private JPanel contentPane;
	public PanelDibujo pnlDibujo;
	private TablaProcesos table;
	private ArrayList<Proceso> procesos = new ArrayList<Proceso>();
	private PanelAdd pnlAgregarProceso;
	private JButton btnSimular;
	private Timer simulacion;
	private boolean bandera = true;
	
	public Ventana() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(10, 10, 870, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(new LineBorder(Color.BLACK));
		scrollPane.setBounds(10, 10, 510, 280);
		contentPane.add(scrollPane);

		scrollPane.setViewportView(pnlDibujo);
		table = new TablaProcesos(procesos);

		scrollPane.setViewportView(table);
		
		pnlAgregarProceso = new PanelAdd();
		pnlAgregarProceso.setBounds(530, 10, 330, 280);
		pnlAgregarProceso.btnAgregar.addActionListener(this);
		contentPane.add(pnlAgregarProceso);
		
		pnlDibujo = new PanelDibujo(40,procesos);
		
		JScrollPane spDibujo = new JScrollPane();
		spDibujo.setBounds(10, 300, 850, 300);
		spDibujo.setBorder(new LineBorder(Color.BLACK));
		spDibujo.setViewportView(pnlDibujo);
		contentPane.add(spDibujo);
		
		btnSimular = new JButton("Simular");
		btnSimular.addActionListener(this);
		btnSimular.setBounds(385, 630, 100, 30);
		add(btnSimular);
	}

	// 				IMPORTANTE
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton) e.getSource();
		if (boton.equals(pnlAgregarProceso.btnAgregar)) {
			long numero = 0;
			if (procesos.size()>0) {
				numero = procesos.get(procesos.size()-1).getTFinal();
			}
			
			Proceso tempo = new Proceso(pnlAgregarProceso.getCampoTexto(0).getText(), Long.parseLong(pnlAgregarProceso.getCampoTexto(1).getText()), Long.parseLong(pnlAgregarProceso.getCampoTexto(2).getText()), numero);
			procesos.add(tempo);
			table.modificarModelo(procesos);
			pnlDibujo.setSize(new Dimension(90 + (50 * (int) (this.procesos.get(this.procesos.size() - 1).getTFinal())), 90 + (50 * (this.procesos.size()+2))));
		}
		else if (boton.equals(btnSimular)) {
			if (bandera) {
				pnlDibujo.setProcesos(procesos);
				simulacion = new Timer(1000, new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						pnlDibujo.repaint();
						pnlDibujo.maximo++;
					}
				});
				btnSimular.setText("Parar");
				simulacion.start();
				bandera = false;
			} else {
				simulacion.stop();
				btnSimular.setText("Simular");
				pnlDibujo.maximo = 0;
				pnlDibujo.repaint();
				bandera = true;
			}
		}
	}
}
