
import java.awt.GraphicsEnvironment;
import java.util.Arrays;

import javax.swing.JOptionPane;

import presentacion.Ventana;

public class Launcher {
	public static void main(String[] args) {
		/*String[] fontNames=GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		System.out.println(Arrays.toString(fontNames));*/
		try {
			Ventana frame = new Ventana();
			frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
